<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Api\DictionaryRepository;
use App\Http\Requests\Api\DictionaryRequest;

class DictionaryController extends Controller
{
    protected $dictionary;

    function __construct(){
        $this->dictionary = new DictionaryRepository();
    }

    public function getDictionaries(){
        $dictionaries = $this->dictionary->getDictionaries();
        return response([
            'status' => true,
            'data' => $dictionaries
        ]);
    }

    public function getDictionary($id){
        $dictionary = $this->dictionary->getDictionary($id);
        return response([
            'status' => true,
            'data' => $dictionary
        ]);
    }

    public function createDictionary(DictionaryRequest $request){
        $dictionary = $this->dictionary->createDictionary();
        return response([
            'status' => true,
            'data' => $dictionary
        ]);
    }

    public function updateDictionary(DictionaryRequest $request, $id){
        $this->dictionary->updateDictionary($id);
        $dictionary = $this->dictionary->getDictionary($id);
        return response([
            'status' => true,
            'data' => $dictionary
        ]);
    }

    public function deleteDictionary($id){
        $this->dictionary->deleteDictionary($id);
        return response([
            'status' => true,
            'message' => ['Dictionary is deleted']
        ]);
    }
}
