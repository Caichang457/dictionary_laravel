<?php
namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class ApiRequest extends FormRequest
{
    protected function failedValidation(Validator $validator): void
    {
        $arr = [];
        $errors = $validator->errors();
        foreach($errors->messages() as $key =>$row){
            $arr[]  = $row[0];
        }

        throw new HttpResponseException(response()->json([
            'status' => false,
            'messages' => $arr
        ], Response::HTTP_OK));
    }
}