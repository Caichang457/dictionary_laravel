<?php

namespace App\Repositories\Api;

use App\Models\Dictionary;

class DictionaryRepository{
	function getDictionaries(){
		return Dictionary::get();
	}

	function getDictionary($id){
		return Dictionary::where('id', $id)->first();
	}

	function createDictionary(){
		return Dictionary::create([
			'word' => Request('word'),
			'description' => Request('description')
		]);
	}

	function updateDictionary($id){
		return Dictionary::where('id', $id)->update([
			'word' => Request('word'),
			'description' => Request('description')
		]);
	}

	function deleteDictionary($id){
		return Dictionary::where('id', $id)->delete();
	}
}