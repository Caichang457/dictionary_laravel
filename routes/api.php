<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\DictionaryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::controller(DictionaryController::class)->group(function(){
    Route::prefix('dictionary')->group(function(){
        Route::get('/', 'getDictionaries');
        Route::get('{id}', 'getDictionary');
        Route::post('create', 'createDictionary');
        Route::put('update/{id}', 'updateDictionary');
        Route::delete('{id}', 'deleteDictionary');
    });
});
